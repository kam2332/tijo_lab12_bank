package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawSpec extends Specification {

    def "should withdraw money if account exists and have enough money"() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.deposit(1, 1)
            bank.createAccount()
            bank.deposit(2, 2)
            bank.createAccount()
            bank.deposit(3, 3)
        when: "try to withdraw"
            def withdrawSuccessful = bank.withdraw(accountNumber, amount)
        then: "money is withdrawn"
            withdrawSuccessful
        where:
            accountNumber | amount
            1             | 1
            2             | 2
            3             | 3
    }

    @Unroll
    def "should not withdraw money #amount from account that doesn't have that amount of money"() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.deposit(1, 1)
            bank.createAccount()
            bank.deposit(2, 2)
            bank.createAccount()
            bank.deposit(3, 3)
        when: "try to withdraw"
            def withdrawSuccessful = bank.withdraw(accountNumber, amount)
        then: "money is not withdrawn"
            !withdrawSuccessful
        where:
            accountNumber | amount
            1             | 10
            2             | 10
            3             | 10
    }
}
