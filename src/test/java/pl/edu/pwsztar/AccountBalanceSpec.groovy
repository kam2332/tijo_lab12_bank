package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountBalanceSpec extends Specification {

    @Unroll
    def "should get account balance account exist" () {
        given: "initial data"
            def bank = new Bank()
            def accountNumber = bank.createAccount()
            bank.deposit(accountNumber, balance)
        when: "get balance of account"
            def accountBalance = bank.accountBalance(accountNumber)
         then: "balance of account should equals #balance"
            accountBalance == result
        where:
            balance        |  result
            500            |  500
            1000           |  1000
            2000           |  2000
            300            |  300
    }

    def "should not get account balance if account does not exist"() {
        given: "initial data"
            def bank = new Bank()
        when: "try to get account balance"
            def accountBalance = bank.accountBalance(1)
        then: "account doesn't exists"
            accountBalance == BankOperation.ACCOUNT_NOT_EXISTS
    }
}
