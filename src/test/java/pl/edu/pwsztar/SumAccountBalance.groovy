package pl.edu.pwsztar

import spock.lang.Specification

class SumAccountBalance extends Specification {

    def "should get balance of all accounts"() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
            bank.createAccount()
            bank.deposit(1, 10)
            bank.deposit(2, 100)
        when: "get sum of accounts balances"
            def balance = bank.sumAccountsBalance()
        then: "get sum of accounts balances"
            balance == 110
    }
}
