package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositSpec extends Specification {

    @Unroll
    def "should be able to deposit money"() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
        when: "deposit money"
            def depositSuccessful = bank.deposit(accountNumber, 1)
        then: "deposit successful"
            depositSuccessful == result
        where:
            accountNumber | result
            1             | true
            2             | true
    }

    def "shouldn't be able to deposit money for account that doesn't exist"() {
        given: "initial data"
            def bank = new Bank()
        when: "deposit money for not existing account"
            def depositSuccessful = bank.deposit(1, 1)
        then: "deposit failed"
            !depositSuccessful
    }
}
