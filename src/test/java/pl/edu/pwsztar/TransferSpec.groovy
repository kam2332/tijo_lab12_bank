package pl.edu.pwsztar

import spock.lang.Specification

class TransferSpec extends Specification {

    def "should be able to transfer money if there is sufficient amount of money on account"() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
            bank.deposit(1, 100)
        when: "try to transfer money"
        def transferSuccessful = bank.transfer(1, 2, 50)
        then: "transfer successful"
            transferSuccessful
    }

    def "should not be able to transfer money if account balance is lower than amount"() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
            bank.deposit(1, 100)
        when: "try to transfer money"
            def transferSuccessful = bank.transfer(1, 2, 150)
        then: "transfer failed"
            !transferSuccessful
    }
}
