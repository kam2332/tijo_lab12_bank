package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification {

    @Unroll
    def "should return balance #balance of deleted account"() {
        given: "initial data"
            def bank = new Bank()
            def accountNumber = bank.createAccount()
            bank.deposit(accountNumber, amount)
        when: "the account is deleted"
            def balanceOfDeletedAccount = bank.deleteAccount(accountNumber)
        then: "check account balance"
            balanceOfDeletedAccount == balance
        where:
            amount  |   balance
            0       |   0
            1       |   1
            100     |   100
    }

    def "shouldn't be able to delete account that doesn't exist"() {
        given: "initial data"
            def bank = new Bank()
        when: "try to delete account that doesn't exist"
            def balanceOfDeletedAccount = bank.deleteAccount(1)
        then: "check account deleted"
            balanceOfDeletedAccount == BankOperation.ACCOUNT_NOT_EXISTS
    }
}
