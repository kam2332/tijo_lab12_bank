package pl.edu.pwsztar;

import java.util.Optional;
import java.util.stream.Collectors;

// TODO: Prosze dokonczyc implementacje oraz testy jednostkowe
// TODO: Prosze nie zmieniac nazw metod - wszystkie inne chwyty dozwolone
// TODO: (prosze jedynie trzymac sie dokumentacji zawartej w interfejsie BankOperation)
class Bank implements BankOperation {

    private static int accountNumber = 0;
    private static final int ZERO_BALANCE = 0;
    private final AccountService accountService;

    public Bank() {
        this.accountService = new AccountRepository();
    }

    public int createAccount() {
        ++accountNumber;
        accountService.addAccount(new Account(accountNumber, ZERO_BALANCE));
        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        int saldo = accountBalance(accountNumber);
        return accountService.deleteAccount(accountNumber) ? saldo : ACCOUNT_NOT_EXISTS;
    }

    public boolean deposit(int accountNumber, int amount) {
        Optional<Account> account = accountService.getAccountByNumber(accountNumber);
        if (account.isPresent()) {
            account.get().setAccountBalance(account.get().getAccountBalance() + amount);
            return true;
        } else {
            return false;
        }
    }

    public boolean withdraw(int accountNumber, int amount) {
        Optional<Account> account = accountService.getAccountByNumber(accountNumber);
        if (account.isEmpty() || account.get().getAccountBalance() < amount) {
            return false;
        } else {
            account.get().setAccountBalance(account.get().getAccountBalance() - amount);
            return true;
        }
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        Optional<Account> fromA = accountService.getAccountByNumber(fromAccount);
        Optional<Account> toA = accountService.getAccountByNumber(toAccount);
        if (fromA.isPresent() && toA.isPresent()) {
            if (fromA.get().getAccountBalance() >= amount) {
                fromA.get().setAccountBalance(fromA.get().getAccountBalance() - amount);
                toA.get().setAccountBalance(toA.get().getAccountBalance() + amount);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public int accountBalance(int accountNumber) {
        final Optional<Account> account = accountService.getAccountByNumber(accountNumber);
        return account.isPresent() ? account.get().getAccountBalance() : ACCOUNT_NOT_EXISTS;
    }

    public int sumAccountsBalance() {
        return accountService.getAllAccounts().stream().mapToInt(Account::getAccountBalance).sum();
    }
}
