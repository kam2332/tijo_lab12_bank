package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AccountRepository implements AccountService {

    List<Account> accounts = new ArrayList<>();

    @Override
    public Optional<Account> getAccountByNumber(int accountNumber) {
        return accounts.stream().findFirst();
    }

    @Override
    public void addAccount(Account account) {
        accounts.add(account);
    }

    @Override
    public boolean deleteAccount(int accountNumber) {
        return accounts.removeIf(account -> account.getAccountNumber() == accountNumber);
    }

    @Override
    public List<Account> getAllAccounts() {
        return accounts;
    }
}
