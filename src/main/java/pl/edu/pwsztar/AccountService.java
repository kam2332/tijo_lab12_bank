package pl.edu.pwsztar;

import java.util.List;
import java.util.Optional;

public interface AccountService {

    Optional<Account> getAccountByNumber(int accountNumber);
    void addAccount(Account account);
    boolean deleteAccount(int accountNumber);
    List<Account> getAllAccounts();
}
